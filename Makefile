#!/usr/bin/make -f
# vim: noet ts=8 ft=makefile

PKG := onionff

ALL_FILES := README TODO install.rdf chrome.manifest \
    $(shell find res -type f) $(shell find chrome -type f)

all: $(PKG).xpi
$(PKG).xpi: $(ALL_FILES)
	rm -f $@
	zip $@ $^

XCF := res/button-bts.xcf

chrome/content/icedeb-trac.png: $(XCF)
	xcf2png -o "$@" "$<" "base" "trac small" "trac big"
chrome/content/icedeb-relay.png: $(XCF)
	xcf2png -o "$@" "$<" "base" "relay small" "relay big"

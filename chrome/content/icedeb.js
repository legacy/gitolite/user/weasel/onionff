OnionFF = {
    getClipboardText: function() {
        var clip = Components.classes['@mozilla.org/widget/clipboard;1']
            .getService(Components.interfaces.nsIClipboard);
        if (!clip) return null;

        var trans = Components.classes['@mozilla.org/widget/transferable;1']
            .createInstance(Components.interfaces.nsITransferable);
        if (!trans) return null;

        trans.addDataFlavor("text/unicode");
        clip.getData(trans,
                clip.supportsSelectionClipboard()
                ? clip.kSelectionClipboard
                : clip.kGlobalClipboard
        );

        var s = new Object;
        var p;
        var len = new Object;
        trans.getTransferData("text/unicode", s, len);

        var text = null;
        var res;

        if (s)
            p = s.value.QueryInterface(Components.interfaces.nsISupportsString, res);
        if (p)
            text = p.data.substring(0, len.value / 2);

        return text;
    },
    trim_keyword: function(word) {
        if (!word) return word;

        var oldword;
        do {
            oldword = word;

            word = word.replace(/^[^a-zA-Z0-9]+/, '');
            word = word.replace(/[^a-zA-Z0-9]+$/, '');
            word = word.replace(/^Bug#/i, '');
        } while ( oldword != word );

        return word;
    },
    lookup_trac: function(in_new) {
        var bug = OnionFF.trim_keyword(OnionFF.getClipboardText());
        if (!bug) return null;
        var uri="https://bugs.torproject.org/" + bug;

        if (in_new) {
            var b = getBrowser();
            var new_tab = b.addTab(uri);
            b.selectedTab = new_tab;
        }
        else {
            loadURI(uri);
        }
    },
    lookup_relay: function(in_new) {
        var fp = OnionFF.trim_keyword(OnionFF.getClipboardText());
        if (!fp) return null;
        var uri="https://metrics.torproject.org/relay-search.html?search=" + fp;

        if (in_new) {
            var b = getBrowser();
            var new_tab = b.addTab(uri);
            b.selectedTab = new_tab;
        }
        else {
            loadURI(uri);
        }
    },

    tracButton: function (e) {
        if ( e.button == 0 )
            OnionFF.lookup_trac(false)
        else if ( e.button == 1 )
            OnionFF.lookup_trac(true);
    },
    relayButton: function (e) {
        if ( e.button == 0 )
            OnionFF.lookup_relay(false)
        else if ( e.button == 1 )
            OnionFF.lookup_relay(true);
    }
}

